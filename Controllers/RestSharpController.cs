using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace payfast_caybo_subs.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RestSharpController : ControllerBase
    {
        const string ISO_8601_DT_FORMAT = "yyyy-MM-dd'T'HH:MM:ss+HH:MM";

        private readonly IConfiguration config;
        public RestSharpController(IConfiguration config)
        {
            this.config = config;
        }

        [HttpGet]
        public async Task<RestResponse> PingPayFastAsync()
        {
            RestClient restClient = new RestClient();
            var request = CreateRequest(getPingEndpoint() + "?testing=false", Method.Get);
            var result = await restClient.GetAsync(request);
            return result;
        }

        private RestRequest CreateRequest(string resource, Method method)
        {
            var request = new RestRequest(resource)
            {
                Method = method,
                RequestFormat = DataFormat.Json                
            };
            var timestamp = DateTime.Now.ToString(ISO_8601_DT_FORMAT);
            request.AddHeader("merchant-id", getMerchantId());
            request.AddHeader("timestamp", timestamp);
            request.AddHeader("version", getAPIVersion());
            request.AddHeader("signature", GenerateSignature(timestamp));
            return request;
        }

        private string GenerateSignature(string timestamp)
        {
            const string amp = "&";
            StringBuilder sb = new StringBuilder();
            string url = 
                "merchant-id=" + UrlEncoderHelper.UrlEncode(getMerchantId()) + amp +
                "passphrase=" + UrlEncoderHelper.UrlEncode(getPassphrase()) + amp +
                "timestamp=" + UrlEncoderHelper.UrlEncode(timestamp) + amp +
                "version=" + UrlEncoderHelper.UrlEncode(getAPIVersion());
            sb.Append(url);
            return StringBuilderHelper.CreateHash(sb);
        }

        private String getPingEndpoint()
        {
            const string configVar = "client:payfast:ping-uri";
            return config.GetValue<string>(configVar) != null ? config.GetValue<string>(configVar) : null;
        }

        private String getMerchantId()
        {
            const string configVar = "client:payfast:merchant-id";
            return config.GetValue<string>(configVar) != null ? config.GetValue<string>(configVar) : null;
        }

        private String getPassphrase()
        {
            const string configVar = "client:payfast:passphrase";
            return config.GetValue<string>(configVar) != null ? config.GetValue<string>(configVar) : null;
        }

        private String getAPIVersion()
        {
            const string configVar = "client:payfast:api-version";
            return config.GetValue<string>(configVar) != null ? config.GetValue<string>(configVar) : null;
        }
    }
}
