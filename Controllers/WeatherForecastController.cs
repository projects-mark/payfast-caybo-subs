﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace payfast_caybo_subs.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        const string ISO_8601_DT_FORMAT = "yyyy-MM-dd'T'HH:MM:ss+HH:MM";
        private readonly IConfiguration _configuration;

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, 
                                         IConfiguration configuration) {
            this._configuration = configuration;
            _logger = logger;
        }

        [HttpGet]
        public async Task<string> PingAsync()
        {
            var response = pingPayFastAsync();
            return await response;
        }

        protected HttpClient constructHttpClient()
        {
            var client = new HttpClient() { BaseAddress = new Uri(getPingEndpoint()) };

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("merchant-id", getMerchantId());
            client.DefaultRequestHeaders.Add("version", getAPIVersion());
            client.DefaultRequestHeaders.Add("timestamp", DateTime.Now.ToString(ISO_8601_DT_FORMAT));
            client.DefaultRequestHeaders.Add("signature", GenerateSignature(client));

            return client;
        }

        private async Task<string> pingPayFastAsync()
        {
            using var client = constructHttpClient();
            var pingEndpointWithTestParam = getPingEndpoint() + "?testing=false";
            using var pingResponse = await client.GetAsync(pingEndpointWithTestParam);
            return HttpStatusCode.OK.Equals(pingResponse.StatusCode) ? await pingResponse.Content.ReadAsStringAsync() : "";
        }

        protected string GenerateSignature(HttpClient httpClient, params KeyValuePair<string, string>[] parameters)
        {
            var dictionary = new SortedDictionary<string, string>();

            foreach (var header in httpClient.DefaultRequestHeaders)
            {
                dictionary.Add(key: header.Key, value: header.Value.First());
            }

            foreach (var keyValuePair in parameters)
            {
                dictionary.Add(key: keyValuePair.Key, value: keyValuePair.Value);
            }

            if (!string.IsNullOrWhiteSpace(getPassphrase()))
            {
                dictionary.Add(key: "passphrase", value: getPassphrase());
            }

            var stringBuilder = new StringBuilder();
            var last = dictionary.Last();

            foreach (var keyValuePair in dictionary)
            {
                stringBuilder.Append($"{keyValuePair.Key.UrlEncode()}={keyValuePair.Value.UrlEncode()}");

                if (keyValuePair.Key != last.Key)
                {
                    stringBuilder.Append("&");
                }
            }
            return stringBuilder != null ? stringBuilder.CreateHash() : String.Empty;
        }

        protected string ComputeMD5Hex(StringBuilder input)
        {
            var inputStringBuilder = new StringBuilder(input.ToString());
            if (!string.IsNullOrWhiteSpace(getPassphrase()))
            {
                inputStringBuilder.Append($"passphrase={UrlEncoderHelper.UrlEncode(getPassphrase())}");
            }

            var md5 = MD5.Create();

            var inputBytes = Encoding.ASCII.GetBytes(inputStringBuilder.ToString());

            var hash = md5.ComputeHash(inputBytes);

            var stringBuilder = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                stringBuilder.Append(hash[i].ToString("x2"));
            }

            return stringBuilder.ToString();
        }

                private String getPingEndpoint() {
            const string configVar = "client:payfast:ping-uri";
            return _configuration.GetValue<string>(configVar) !=  null ? _configuration.GetValue<string>(configVar) : string.Empty;
        }

        private String getMerchantId() {
            const string configVar = "client:payfast:merchant-id";
            return _configuration.GetValue<string>(configVar) !=  null ? _configuration.GetValue<string>(configVar) : string.Empty;
        }

        private String getPassphrase() {
            const string configVar = "client:payfast:passphrase";
            return _configuration.GetValue<string>(configVar) != null ? _configuration.GetValue<string>(configVar) : string.Empty;
        }

        private String getAPIVersion()
        {
            const string configVar = "client:payfast:api-version";
            return _configuration.GetValue<string>(configVar) != null ? _configuration.GetValue<string>(configVar) : string.Empty;
        }
    }
}